CREATE TABLE account (
  id          SERIAL NOT NULL UNIQUE PRIMARY KEY,
  name        TEXT   NOT NULL,
  slug        TEXT   NOT NULL UNIQUE
);

CREATE TABLE shelf (
  id          SERIAL NOT NULL UNIQUE PRIMARY KEY,
  slug        TEXT   NOT NULL UNIQUE,
  description TEXT   NOT NULL
);

CREATE TABLE shelf_seller (
  shelf_id  SERIAL NOT NULL UNIQUE references shelf (id),
  seller_id SERIAL NOT NULL references account (id)
);

CREATE TABLE bag (
  id SERIAL NOT NULL UNIQUE PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE bag_buyer (
  bag_id   SERIAL NOT NULL UNIQUE references bag (id),
  buyer_id SERIAL NOT NULL references account (id)
);

CREATE TABLE product (
  id    SERIAL NOT NULL UNIQUE PRIMARY KEY,
  name  TEXT   NOT NULL,
  price REAL   NOT NULL
);

CREATE TABLE product_shelf (
  product_id SERIAL NOT NULL UNIQUE references product (id),
  shelf_id   SERIAL NOT NULL references shelf (id)
);

CREATE TABLE bag_item (
  id     SERIAL NOT NULL UNIQUE PRIMARY KEY,
  amount REAL   NOT NULL
);

CREATE TABLE bag_item_bag (
  bag_item_id SERIAL NOT NULL UNIQUE references bag_item (id),
  bag_id      SERIAL NOT NULL references bag (id)
);

CREATE TABLE bag_item_product (
  bag_item_id SERIAL NOT NULL UNIQUE references bag_item (id),
  product_id  SERIAL NOT NULL references product (id)
);

CREATE VIEW bag_shelf AS
  SELECT bag.id AS bag_id, shelf.id AS shelf_id
  FROM bag
         INNER JOIN bag_item_bag ON bag.id = bag_item_bag.bag_id
         INNER JOIN bag_item ON bag_item_bag.bag_item_id = bag_item.id
         INNER JOIN bag_item_product ON bag_item.id = bag_item_product.bag_item_id
         INNER JOIN product_shelf ON bag_item_product.product_id = product_shelf.product_id
         INNER JOIN shelf ON product_shelf.shelf_id = shelf.id;
