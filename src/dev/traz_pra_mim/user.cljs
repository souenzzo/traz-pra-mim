(ns traz-pra-mim.user
  (:require [traz-pra-mim.client :as client]))

(defn after-load
  []
  (let [target (client/document->target js/document)]
    (client/render target)))
