(ns user
  (:require [shadow.cljs.devtools.server :as shadow.server]
            [server.core :as server]
            [shadow.cljs.devtools.api :as shadow]
            [io.pedestal.http :as http]))


(defonce shadow-server
         (delay
           (shadow.server/start!)))

(defonce http-state (atom nil))

(defn -main
  [& args]
  (swap! http-state (fn [x]
                      (when x
                        (http/stop x))
                      (-> (server/service {::server/http-port 8080})
                          (assoc :env :dev)
                          http/default-interceptors
                          http/dev-interceptors
                          http/create-server
                          http/start)))
  (prn [@shadow-server])
  (shadow/watch :main)
  (shadow/watch :cards))
