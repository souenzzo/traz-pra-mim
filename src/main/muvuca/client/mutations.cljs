(ns muvuca.client.mutations
  (:require [fulcro.client.mutations :as m]))

(defmulti mutation-merge (fn [state mutation-sym return-value] mutation-sym))
(defmethod mutation-merge :default
  [state mutation-sym return-value]
  state)


(m/defmutation muvuca.bag-item/dec-amount
  [{:muvuca.bag-item/keys [id]}]
  (action [{:keys [state]}]
          (swap! state update-in [:muvuca.bag-item/by-id id :muvuca.bag-item/amount] dec))
  (remote [_] true))

(defmethod mutation-merge `muvuca.bag-item/dec-amount
  [state mutation-sym {:muvuca.bag-item/keys [id amount]}]
  (assoc-in state [:muvuca.bag-item/by-id id :muvuca.bag-item/amount] amount))

(m/defmutation muvuca.bag-item/inc-amount
  [{:muvuca.bag-item/keys [id]}]
  (action [{:keys [state]}]
          (swap! state update-in [:muvuca.bag-item/by-id id :muvuca.bag-item/amount] inc))
  (remote [_] true))

(defmethod mutation-merge `muvuca.bag-item/inc-amount
  [state mutation-sym {:muvuca.bag-item/keys [id amount]}]
  (assoc-in state [:muvuca.bag-item/by-id id :muvuca.bag-item/amount] amount))

(m/defmutation muvuca.bag/confirm
  [{:muvuca.bag/keys [id]}]
  (action [{:keys [state]}]
          (swap! state assoc-in [:muvuca.bag/by-id id :muvuca.bag/confirmed?] true))
  (remote [_] true))
