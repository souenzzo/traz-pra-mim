(ns traz-pra-mim.pages
  (:require [fulcro.client.primitives :as fp]
            [fulcro.client.dom :as dom]
            [fulcro.client.mutations :as m]))

(defn form
  [{{:keys [on-click label]} :submit
    :keys                    [inputs]}]
  (fp/fragment
    (map-indexed
      (fn [idx {:keys [value label on-change]}]
        (fp/fragment
          {:key idx}
          (dom/label label)
          (dom/input {:onChange #(on-change (-> % .-target .-value))
                      :value    value})))
      inputs)
    (dom/button
      {:onClick on-click}
      label)))


(fp/defsc Login [this {::keys   [id ident]
                       :ui/keys [username password]}]
  {:initial-state (fn [_] {::id         ::login
                           ::ident      ::login
                           :ui/username ""
                           :ui/password ""})
   :ident         (fn [] [id ident])
   :query         [::id
                   ::ident
                   :ui/username
                   :ui/password]}
  (dom/div
    (form {:inputs [{:value     username
                     :on-change #(m/set-value! this :ui/username %)
                     :label     "Username"}
                    {:value     password
                     :on-change #(m/set-value! this :ui/password %)

                     :label     "Password"}]
           :submit {:on-click #(fp/transact! this `[(login ~{:username username
                                                             :password password})])
                    :label    "Login!"}})))

(fp/defsc CurrentBagItem [this {:muvuca.bag-item/keys [amount id]}]
  {:ident         [:muvuca.bag-item/by-id :muvuca.bag-item/id]
   :query         [:muvuca.bag-item/id
                   :muvuca.bag-item/amount]
   :initial-state (fn [_]
                    {:muvuca.bag-item/id     0
                     :muvuca.bag-item/amount 3})}

  (dom/div
    (dom/button {:onClick #(fp/transact! this `[{(muvuca.bag-item/dec-amount ~{:muvuca.bag-item/id id})
                                                 [:muvuca.bag-item/id
                                                  :muvuca.bag-item/amount]}])}
                "-")
    (str amount)
    (dom/button {:onClick #(fp/transact! this `[{(muvuca.bag-item/inc-amount ~{:muvuca.bag-item/id id})
                                                 [:muvuca.bag-item/id
                                                  :muvuca.bag-item/amount]}])}
                "+")))

(def ui-current-bag-item (fp/factory CurrentBagItem {:keyfn :muvuca.bag-item/id}))

(fp/defsc BuyerProduct [this {:muvuca.product/keys [id name current-bag-item]}]
  {:initial-state (fn [{:keys [id name]}] {:muvuca.product/id               id
                                           :muvuca.product/current-bag-item (fp/get-initial-state CurrentBagItem {})
                                           :muvuca.product/name             (or name
                                                                                (str "Produto " id))})
   :ident         [:muvuca.product/by-id :muvuca.product/id]
   :query         [:muvuca.product/id
                   :muvuca.product/name
                   {:muvuca.product/current-bag-item (fp/get-query CurrentBagItem)}]}
  (dom/div (str id " - " name)
           (ui-current-bag-item current-bag-item)))

(def ui-buyer-product (fp/factory BuyerProduct {:keyfn :muvuca.product/id}))

(fp/defsc CurrentBag [this {:muvuca.bag/keys [id amount]}]
  {:query [:muvuca.bag/id
           :muvuca.bag/amount
           :muvuca.bag/confirmed?]
   :ident [:muvuca.bag/by-id :muvuca.bag/id]}
  (dom/div
    (str amount)
    (dom/button {:onClick #(fp/transact! this `[(muvuca.bag/confirm ~{:id id})])}
                "OK")))

(def ui-current-bag (fp/factory CurrentBag))

(fp/defsc BuyerShelf [this {:muvuca.shelf/keys [products
                                                current-bag
                                                description]}]
  {:initial-state (fn [_] {:muvuca.shelf/id          123
                           :muvuca.shelf/slug        "bananas-do-kelvin"
                           :muvuca.shelf/title       "Bananas do kelvin"
                           :muvuca.shelf/description "Bananas organicas colhidas em Maricá"
                           :muvuca.shelf/products    (mapv #(fp/get-initial-state BuyerProduct {:id %})
                                                           (range 10))})
   :ident         [:muvuca.shelf/by-id :muvuca.shelf/id]
   :query         [:muvuca.shelf/id
                   :muvuca.shelf/slug
                   :muvuca.shelf/description
                   :muvuca.shelf/current-bag (fp/get-query CurrentBag)
                   {:muvuca.shelf/products (fp/get-query BuyerProduct)}]}
  (fp/fragment
    description
    (map ui-buyer-product products)
    (ui-current-bag current-bag)))

(def ui-buyer-shelf (fp/factory BuyerShelf {:keyfn :muvuca.shelf/id}))

(fp/defsc Shelf [this {:keys  [ui/shelf]
                       ::keys [id ident]}]
  {:query         [::id ::ident {:ui/shelf (fp/get-query BuyerShelf)}]
   :ident         (fn [] [id ident])
   :initial-state (fn [_]
                    {::id      ::shelf
                     ::ident   ::shelf
                     :ui/shelf (fp/get-initial-state BuyerShelf {})})}
  (ui-buyer-shelf shelf))

(fp/defsc Loading [this {::keys [id ident]}]
  {:query         [::id ::ident]
   :ident         (fn [] [id ident])
   :initial-state (fn [_]
                    {::id    ::loading
                     ::ident ::loading})}
  (dom/div "Loading ..."))


