(ns traz-pra-mim.client
  (:require [goog.object :as gobj]
            [fulcro.client.primitives :as fp]
            [fulcro.client.routing :as r]
            [goog.dom :as gdom]
            [traz-pra-mim.pages :as pages]
            [goog.events :as ev]
            [goog.history.EventType :as event-type]
            [fulcro.client :as fc]
            [fulcro.client.dom :as dom]
            [fulcro.client.mutations :as m]
            [muvuca.client.mutations :as client.mutations]
            [cognitect.transit :as t]
            [fulcro.client.network :as net])
  (:import (goog History)))

(defonce fulcro-app (atom nil))

(r/defsc-router Root [this {::pages/keys [id ident]
                            :as          props}]
  {:router-targets {::pages/loading pages/Loading
                    ::pages/shelf   pages/Shelf
                    ::pages/login   pages/Login}
   :ident          (fn [] [ident id])
   :router-id      :root
   :default-route  pages/Shelf}
  (fp/fragment
    (dom/div "404")
    (dom/code (pr-str props))))

(defn render
  [target]
  (swap! fulcro-app fc/mount Root target))

(m/defmutation page/login
  [{:keys []}]
  (action [{:keys [state]}]
          (swap! state assoc ::r/current-route [::pages/login ::pages/login])))

(defn on-nav
  [{:keys [reconciler]} nav]
  (let [token (.-token nav)]
    #_(fp/transact! reconciler `[(page/login ~{})])))

(defn start
  [{:keys [anti-forgery-token]
    :as   state} target]
  (let [history (new History)
        client (fc/new-fulcro-client
                 :shared (assoc state
                           :history history)
                 :mutation-merge client.mutations/mutation-merge
                 :request-transform (net/wrap-csrf-token anti-forgery-token)
                 :started-callback (fn [app]
                                     (ev/listen history event-type/NAVIGATE (partial on-nav app))
                                     (.setEnabled history true)))]

    (reset! fulcro-app client)
    (render target)))

(defn document->target
  [doc]
  (let [target-id (gobj/getValueByKeys doc "body" "dataset" "targetId")]
    (gdom/getElement target-id)))
(def transit-handlers
  {:read {"n"     (fn n-transit-handlers [v] (js/parseInt v 10))
          "u"     (fn u-transit-handlers [v] (uuid v))
          "ratio" (fn ratio-transit-handlers [v] (/ (first v) (last v)))
          "f"     (fn f-transit-handlers [v] (js/parseFloat v))}})

(def transit-reader
  (t/reader :json {:handlers (:read transit-handlers)}))

(defn document->state
  [doc]
  (let [state-str (gobj/getValueByKeys doc "body" "dataset" "state")]
    (t/read transit-reader state-str)))



(defn ^:export -main
  []
  (let [target (document->target js/document)
        state (document->state js/document)]
    (start state target)))

(defonce running (-main))
