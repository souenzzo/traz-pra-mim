(ns server.core
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [cognitect.transit :as transit]
            [fulcro.client.dom-server :as dom]
            [fulcro.client.primitives :as fp]
            [io.pedestal.http :as http]
            [io.pedestal.http.csrf :as csrf]
            [io.pedestal.log :as log]
            [ring.middleware.resource :as resource]
            [ring.util.mime-type :as mime]
            [com.wsscode.pathom.core :as p]
            [com.wsscode.pathom.connect :as pc]
            [datascript.core :as d])
  (:import (java.io File ByteArrayOutputStream)
           (org.eclipse.jetty.server.handler.gzip GzipHandler)
           (org.eclipse.jetty.servlet ServletContextHandler)))

(def verbose-transit? true)
(s/def ::transit-type #{:msgpack :json :json-verbose})

(defonce idx-bag-item
         (atom {}))

(pc/defmutation confirm
  [{:keys [conn]} {:muvuca.bag/keys [id]}]
  {::pc/sym    `muvuca.bag/confirm
   ::pc/params [:muvuca.bag/id]
   ::pc/output [:muvuca.bag/id]}
  (let [tx-data [{:muvuca.bag/id         id
                  :muvuca.bag/confirmed? true}]
        {:keys [db-after]} @(d/transact conn tx-data)]
    {:muvuca.bag/id id}))

(defn pull-resolver
  [{::pc/keys [resolver-data] :keys [conn]} input]
  (let [pk (first (::pc/input resolver-data))
        pattern (into [:db/id] (::pc/output resolver-data))
        eid (find input pk)]
    (d/pull (d/db conn) pattern eid)))

(pc/defresolver muvuca-bag
  [ctx input]
  {::pc/input  #{:muvuca.bag/id}
   ::pc/output [:muvuca.bag/id
                :muvuca.bag/amount
                :muvuca.bag/confirmed?]}
  (pull-resolver ctx input))

(pc/defresolver muvuca-shelf
  [ctx input]
  {::pc/input  #{:muvuca.shelf/slug}
   ::pc/output [:muvuca.shelf/id
                :muvuca.shelf/slug
                :muvuca.shelf/description
                {:muvuca.shelf/products [:muvuca.product/id]}]}
  (pull-resolver ctx input))

(pc/defresolver muvuca-product
  [ctx input]
  {::pc/input  #{:muvuca.product/id}
   ::pc/output [:muvuca.product/id
                :muvuca.product/name
                :muvuca.product/price]}
  (doto (pull-resolver ctx input) prn))


(pc/defmutation inc-amount
  [env {:muvuca.bag-item/keys [id]}]
  {::pc/sym    `muvuca.bag-item/inc-amount
   ::pc/params [:muvuca.bag-item/id]
   ::pc/output [:muvuca.bag-item/id
                :muvuca.bag-item/amount]}
  (-> (swap! idx-bag-item update id (fn [{:muvuca.bag-item/keys [amount]
                                          :as                   bag-item}]
                                      (if (number? amount)
                                        (assoc bag-item
                                          :muvuca.bag-item/amount (inc amount))
                                        {:muvuca.bag-item/amount 1
                                         :muvuca.bag-item/id     id})))
      (get id)))

(pc/defmutation dec-amount
  [env {:muvuca.bag-item/keys [id]}]
  {::pc/sym    `muvuca.bag-item/dec-amount
   ::pc/params [:muvuca.bag-item/id]
   ::pc/output [:muvuca.bag-item/id
                :muvuca.bag-item/amount]}
  (-> (swap! idx-bag-item update id (fn [{:muvuca.bag-item/keys [amount]
                                          :as                   bag-item}]
                                      (if (number? amount)
                                        (assoc bag-item
                                          :muvuca.bag-item/amount (dec amount))
                                        {:muvuca.bag-item/amount -1
                                         :muvuca.bag-item/id     id})))
      (get id)))



(def registers
  [dec-amount
   muvuca-bag
   muvuca-shelf
   muvuca-product
   confirm
   inc-amount])

(def schema {:muvuca.bag/id         {:db/unique :db.unique/identity}
             :muvuca.shelf/slug     {:db/unique :db.unique/identity}
             :muvuca.shelf/id       {:db/unique :db.unique/identity}
             :muvuca.shelf/products {:db/valueType   :db.type/ref
                                     :db/cardinality :db.cardinality/many}
             :muvuca.product/id     {:db/unique :db.unique/identity}
             :muvuca.bag-item/id    {:db/unique :db.unique/identity}})
(def tx-init-data
  (into []
        cat
        (let [shelf-id (d/tempid :db.part/user)]
          [[{:db/id                    shelf-id
             :muvuca.shelf/id          (d/squuid)
             :muvuca.shelf/slug        "foobar"
             :muvuca.shelf/description "Foobar"}]
           (for [i (range 10)]
             {:muvuca.product/id      (d/squuid)
              :muvuca.product/name    (rand-nth (for [i #{"Banana" "Feijão" "Batata" "Morango" "Arroz"}
                                                      j #{"Organica" "Integral" "da terra"}]
                                                  (string/join " " [i j])))

              :muvuca.product/price   (rand-nth [42 21 33])
              :muvuca.shelf/_products shelf-id})])))
(defonce conn (doto (d/create-conn schema)
                (d/transact tx-init-data)))

(def parser
  (p/parser {::p/env     {:conn                    conn
                          ::p/reader               [p/map-reader
                                                    pc/reader2
                                                    pc/open-ident-reader
                                                    p/env-placeholder-reader]
                          ::p/placeholder-prefixes #{">"}}
             ::p/mutate  pc/mutate
             ::p/plugins [(pc/connect-plugin {::pc/register registers})
                          p/error-handler-plugin]}))

(defn handler-parser
  [{::csrf/keys [anti-forgery-token]
    :keys       [transit-params edn-params]
    :as         request}]
  (let [params (or transit-params edn-params)
        result (parser request params)]
    (log/debug :anti-forgery-token anti-forgery-token :query params :result result)
    {:body   result
     :status 200}))

(defn pr-transit
  [type body]
  (fn pr-transit [out]
    (try
      (let [writer (transit/writer out type)]
        (transit/write writer body))
      (catch Throwable e
        (log/error :pr-transit e)))))

(defn pr-transit-str
  [type body]
  (let [write! (pr-transit type body)]
    (-> (doto (new ByteArrayOutputStream)
          (write!))
        str)))

(s/fdef pr-transit
        :args (s/cat :type ::transit-type
                     :body any?)
        :ret fn?)

(defn transit-type
  ([x] (transit-type x false))
  ([x verbose?]
   (when (and (string? x)
              (or (string/starts-with? x "application/transit")
                  (= x "*/*")))
     (cond
       (string/ends-with? (str x) "+msgpack") :msgpack
       verbose? :json-verbose
       :else :json))))

(s/fdef transit-type
        :args (s/cat :x (s/nilable string?)
                     :in (s/? boolean?))
        :ret #{:msgpack :json :json-verbose})

(def type->conten-type
  {:json         "application/transit+json"
   :json-verbose "application/transit+json"
   :msgpack      "application/transit+msgpack"
   :edn          "application/edn"})

(def ->edn
  {:name  ::->edn
   :leave (fn [{{{:strs [accept]} :headers} :request
                {:keys [body]}              :response
                :as                         ctx}]
            (let [type (transit-type accept verbose-transit?)
                  response-body (if type
                                  (pr-transit type body)
                                  (pr-str body))]
              (-> ctx
                  (assoc-in [:response :body] response-body)
                  (assoc-in [:response :headers "Content-Type"] (type->conten-type (or type :edn))))))})

(defn handler-resources
  [{:keys [path-info uri]}]
  (log/debug :path-info path-info :uri uri)
  (let [rm-static #(string/replace (str %) #"^/_static" "")]
    (resource/resource-request
      {:request-method :get
       :path-info      (rm-static path-info)
       :uri            (rm-static uri)}
      "/public")))

(defn file?
  [x]
  (instance? File x))

(def +mime-types
  {:name  ::+mime-types
   :leave (fn [{{:keys [body]} :response
                :as            ctx}]
            (if (file? body)
              (let [filename (.getName ^File body)
                    path [:response :headers "Content-Type"]
                    type (cond
                           (= filename "/") "text/html; charset=utf-8"
                           :else (mime/ext-mime-type filename))]
                (cond-> ctx
                        (string? filename) (assoc-in path type)))
              ctx))})

(defn +env
  [env]
  {:name  ::+env
   :enter (fn [ctx] (update ctx :request merge env))})

(fp/defsc Index
  [this {:keys [transit-type state]}]
  {}
  (dom/html
    (dom/head
      (dom/meta {:charset "UTF-8"})
      (dom/link {:id   "favicon"
                 :rel  "shortcut icon"
                 :type "image/png"
                 :href (str "data:image/svg," (dom/render-to-str (dom/svg)))})
      (dom/title "Traz pra mim"))
    (dom/body
      {:data-target-id "app"
       :data-state     (pr-transit-str transit-type state)}
      (dom/div {:id "app"})
      (dom/script {;; TODO: :integrity (-> (io/file "public/js/main/main.js") sha384 ->base64)
                   ;; TODO: :crossorigin "anonymous"
                   :src (str "/_static/js/main/main.js")}))))
(def ui-index (fp/factory Index))

(defn index-page
  [{::csrf/keys [anti-forgery-token]
    :as         ctx}]
  (let [transit-type (if verbose-transit? :json-verbose :json)
        state {:anti-forgery-token anti-forgery-token}]
    {:headers {"Cache-Control" "no-cache"
               "Content-Type"  "text/html; charset=utf-8"}
     :body    (->> ["<!DOCTYPE html>"
                    (dom/render-to-str (ui-index {:transit-type transit-type
                                                  :state        state}))]
                   (string/join "\n"))
     :status  200}))

(defn routes
  [env]
  `#{["/api" :post [~(+env env) ->edn ~(csrf/anti-forgery) handler-parser]]
     ["/_static/*path" :get [+mime-types ~(+env env) handler-resources]]
     ["/" :get [~(+env env) ~(csrf/anti-forgery) index-page]]})


(defn context-configurator
  "Habilitando gzip nas respostas"
  [^ServletContextHandler context]
  (let [gzip-handler (GzipHandler.)]
    (.setExcludedAgentPatterns gzip-handler (make-array String 0))
    (.setGzipHandler context gzip-handler))
  context)

(def content-security-policy-settings
  ["script-src"
   ;; unssafe-eval just for dev
   "'unsafe-eval'"
   "'self'"])

(defn service
  [{::keys [http-port join? http-host]
    :as    env}]
  {:env                     :prod
   ::http/routes            (routes env)
   ::http/port              http-port
   ::http/join?             join?
   ::http/type              :jetty
   ::http/enable-csrf       {}
   ::http/enable-session    {}
   ::http/secure-headers    {:content-security-policy-settings (string/join " " content-security-policy-settings)}
   ::http/host              http-host
   ::http/allowed-origins   {:creds           true
                             :allowed-origins (constantly true)}
   ::http/container-options (cond-> {:h2c?                 true
                                     :context-configurator context-configurator})})
