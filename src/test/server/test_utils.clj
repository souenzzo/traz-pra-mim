(ns server.test-utils
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]))


(def db
  {:dbtype   "postgresql"
   :dbname   "app"
   :host     "localhost"
   :user     "postgres"
   :password "postgres"})

(defn default-fixture [f]
  (jdbc/execute! (assoc db :dbname "")
                 "DROP DATABASE IF EXISTS app;" {:transaction? false})
  (jdbc/execute! (assoc db :dbname "")
                 "CREATE DATABASE app;" {:transaction? false})
  (jdbc/execute! db (slurp (io/resource "schema/000-base.sql")))
  (f))
