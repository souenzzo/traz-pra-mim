(ns server.core-test
  (:require [clojure.test :refer [deftest use-fixtures]]
            [server.test-utils :as test-utils]
            [server.core :as server]
            [walkable.sql-query-builder :as sqb]
            [midje.sweet :refer :all])
  (:import (java.time Instant)
           (java.sql Timestamp)))

(use-fixtures :once test-utils/default-fixture)

(deftest min-test
  (let [db test-utils/db
        ctx {::sqb/sql-db db :db db ::server/db db}
        user-id (atom nil)
        product-id (atom nil)]
    (fact
      "Criando conta"
      (-> (server/parser ctx `[(account/create ~{:account/name "Enzzo"
                                                 :account/slug "123"})
                               {[:account/by-slug "123"] [:account/name
                                                          :account/token
                                                          :account/id
                                                          :account/slug]}])
          (get [:account/by-slug "123"]))
      => (contains {:account/slug  "123"
                    :account/token "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyLWlkIjoxfQ.7HEnAQc9Q2-xWvbNbWuniHO346YLOub9o252Y2RcQP0"
                    :account/id    #(number? (reset! user-id %))
                    :account/name  "Enzzo"}))
    (fact
      "Criando estante"
      (-> (server/parser ctx `[(shelf/create ~{:account/slug      "123"
                                               :shelf/slug        "my-shelf"
                                               :shelf/description "Queijo de Minas"})
                               {[:shelf/by-slug "my-shelf"] [:shelf/id
                                                             {:shelf/seller [:account/name]}
                                                             :shelf/slug
                                                             :shelf/description]}])
          (get [:shelf/by-slug "my-shelf"]))
      => (contains {:shelf/description "Queijo de Minas"
                    :shelf/id          number?
                    :shelf/seller      (contains {:account/name "Enzzo"})
                    :shelf/slug        "my-shelf"}))
    (fact
      "Criando produto"
      (-> (server/parser ctx `[(product/create ~{:shelf/slug    "my-shelf"
                                                 :product/name  "Quejo"
                                                 :product/price 10})
                               {[:shelf/by-slug "my-shelf"] [:shelf/id
                                                             {:shelf/products [:product/id
                                                                               :product/name]}]}])
          (get [:shelf/by-slug "my-shelf"])
          :shelf/products
          first)
      => (contains {:product/id   #(number? (reset! product-id %))
                    :product/name "Quejo"}))
    (fact
      "Vendo uma estante"
      (-> (server/parser ctx [{[:shelf/by-slug "my-shelf"] [:shelf/description
                                                            {:shelf/seller [:account/name
                                                                            :account/slug]}
                                                            {:shelf/products [:product/id
                                                                              :product/name
                                                                              :product/price]}]}])
          (get [:shelf/by-slug "my-shelf"]))
      => (contains {:shelf/description "Queijo de Minas"
                    :shelf/seller      {:account/name "Enzzo"
                                        :account/slug "123"}
                    :shelf/products    (just #{(contains {:product/id    @product-id
                                                          :product/name  "Quejo"
                                                          :product/price number?})})}))
    #_(fact
        "Pedindo um item"
        (-> (server/parser (assoc ctx
                             :timestamp (Timestamp/from (Instant/now))
                             :user-id @user-id)
                           `[(product/buy ~{:bag-item/amount 33
                                            :product/id      @product-id})
                             {[:account/by-slug "123"] [:account/name
                                                        {:account/bags [:bag/id
                                                                        :bag/created-at
                                                                        {:bag/shelf [:shelf/slug]}
                                                                        {:bag/items [:bag-item/amount]}]}]}])
            (get [:account/by-slug "123"])
            (get :account/bags)
            first)
        => (contains {:bag/items      (just #{(contains {:bag-item/amount 33.0})})
                      :bag/created-at inst?
                      :bag/shelf      (contains {:shelf/slug "my-shelf"})}))))
