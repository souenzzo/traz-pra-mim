# Running for tests
First make a postgres availble on `localhost:5432` with username `postgres` and password `postgres`

```bash
docker run --rm -p 5432:5432 postgres:alpine
```

Install `yarn` and `clj`

```bash
$ which yarn clj
/usr/bin/yarn
/usr/bin/clj
# or install via pacman -S clojure yarn
```

Open a repl
```bash
clj -Sdeps '{:deps {leiningen/leiningen {:mvn/version "2.8.1"}}}' -m leiningen.core.main fullstack
## or use lein:
## lein fullstack
```
Then type on repl
```clojure
;; start the frontend on http://localhost:3449 (slow)
(user/start)
;; start the backend on http://localhost:8000 (fast)
(user/restart)
;; run the integration test to install table/db/schema and
;; get some "background" data
(user/run-test)
```

Now open your browser in `http://localhost:3449` and login as `123`

Also checkout `devcards` at `http://localhost:3449/cards.html`
