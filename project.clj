(defproject souenzzo/muvuca "0.1.0-SNAPSHOT"
  :source-paths ["src/main" "src/dev" "src/test" "src/workspaces"]
  :dependencies [[bidi/bidi "2.1.5"]
                 [binaryage/devtools "0.9.10"]
                 [buddy/buddy-sign "3.0.0"]
                 [cider/piggieback "0.4.0"]
                 [clj-http/clj-http "3.9.1"]
                 [com.cognitect/transit-clj "0.8.313"]
                 [com.cognitect/transit-cljs "0.8.256"]
                 [com.wsscode/pathom "2.2.9"]
                 [com.google.javascript/closure-compiler-unshaded "v20190121"]
                 [fulcrologic/fulcro "2.8.1"]
                 [cheshire/cheshire "5.8.1"]
                 [fulcrologic/fulcro-inspect "2.2.5"]
                 [io.pedestal/pedestal.jetty "0.5.5"]
                 [com.fasterxml.jackson.core/jackson-core "2.9.8"]
                 [io.pedestal/pedestal.service "0.5.5"]
                 [nubank/workspaces "1.0.4"]
                 [org.clojure/clojure "1.10.0"]
                 [datascript/datascript "0.17.1"]
                 [org.clojure/clojurescript "1.10.520"]
                 [org.clojure/core.async "0.4.490"]
                 [org.clojure/java.jdbc "0.7.8"]
                 [org.clojure/test.check "0.10.0-alpha3"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [org.postgresql/postgresql "42.2.5"]
                 [ch.qos.logback/logback-classic "1.3.0-alpha4"]
                 [re-frame/re-frame "0.10.6"]
                 [reagent/reagent "0.8.1"]
                 [thheller/shadow-cljs "2.7.36"]
                 [walkable/walkable "1.2.0-snapshot"]]
  :repl-options {:nrepl-middleware [shadow.cljs.devtools.server.nrepl/cljs-load-file
                                    shadow.cljs.devtools.server.nrepl/cljs-eval
                                    shadow.cljs.devtools.server.nrepl/cljs-select
                                    cider.piggieback/wrap-cljs-repl]})
